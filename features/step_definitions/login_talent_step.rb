require_relative '../support/screen_action.rb'

Given("user open leave.tujuhsembilan.com") do
    maximize_browser()
    open_url("https://leave.tujuhsembilan.com/#/")
    sleep(5)
    #screenshot("homepage_annual_leave")
  end
  
  Then("user talent input username {string}") do |keyword|
    input_username_field(keyword)
    sleep(2)
    #screenshot("keyword_username")
  end

  Then("user talent input password {string}") do |keyword|
    input_password_field(keyword)
    sleep(2)
    #screenshot("keyword_password")
  end

  Then("user talent klik tombol login annual leave") do
    login_button()
    sleep(2)
    #screenshot("login_button")
  end