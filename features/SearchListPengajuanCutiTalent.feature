@SearchListPengajuanCuti
Feature: Searching List Pengajuan

@SearchDeskripsiTalent
Scenario Outline:  User Mencari Deskripsi Pengajuan Cuti Talent
Given user open leave.tujuhsembilan.com
Then user talent input username "talenttest1"
Then user talent input password "TestMuluNich!"
Then user talent klik tombol login annual leave
Then user talent input search "testing ambil cuti 3 hari"
Then user talent klik icon search talent

@SearchTanggalPengajuanTalent
Scenario Outline:  User Mencari Tanggal Pengajuan Cuti Talent
Given user open leave.tujuhsembilan.com
Then user talent input username "talenttest1"
Then user talent input password "TestMuluNich!"
Then user talent klik tombol login annual leave
Then user talent input search "23 Jun 2023"
Then user talent klik icon search talent

@SearchTanggalCutiTalent
Scenario Outline:  User Mencari Tanggal Cuti Talent
Given user open leave.tujuhsembilan.com
Then user talent input username "talenttest1"
Then user talent input password "TestMuluNich!"
Then user talent klik tombol login annual leave
Then user talent input search "18 Jul 2023"
Then user talent klik icon search talent

@SearchStatusPengajuanTalent
Scenario Outline:  User Mencari Tanggal Cuti Talent
Given user open leave.tujuhsembilan.com
Then user talent input username "talenttest1"
Then user talent input password "TestMuluNich!"
Then user talent klik tombol login annual leave
Then user talent input search "Canceled"
Then user talent klik icon search talent