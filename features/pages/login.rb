$input_username = '//*[@id="horizontal_login_userName"]'
$input_password = '//*[@id="horizontal_login_password"]'
$login_btn = '//*[@id="app"]/div/div/div/form/div/div[3]/div/div/span/button'

def input_username_field(keyword)
    $browser.find_element(:xpath,$input_username).send_keys(keyword)
end

def input_password_field(keyword)
    $browser.find_element(:xpath,$input_password).send_keys(keyword)
end

def login_button()
    $browser.find_element(:xpath,$login_btn).click
end