$input_search = '//*[@id="searchInput"]'
$icon_search = '//*[@id="app"]/div/div[2]/div[2]/div[1]/div/span/span/i'

def input_search_talent(keyword)
    $browser.find_element(:xpath,$input_search).send_keys(keyword)
end

def icon_search_talent()
    $browser.find_element(:xpath,$icon_search).click
end